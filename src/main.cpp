#include <iostream>
#include <string>

#include "promise/interface/Promise.hpp"

int main() {
	std::cout << "pre" << std::endl;
	std::string test = "test string for demo";

	(new Promise<>([&test](IPromise<>& promise) {
		std::string a  = "test string for demo";
		promise.resolve(a);
	}))->then([](std::string res) {
		std::cout << res << " 1" << std::endl;
	});

	(new Promise<>([](IPromise<>& promise) {
		promise.reject("ERROR!! 2");
	}))->then([](std::string res) {
		std::cout << res << std::endl;
	}, [](std::string res) {
		std::cout << res << std::endl;
	});

	(new Promise<>([](IPromise<>& promise) {
		promise.resolve("ok 3");
		promise.reject("errore strano 3");
	}))->then([](std::string res) {
		std::cout << res << std::endl;
	});

	std::cout << "post" << std::endl;
	
	return 0;
}