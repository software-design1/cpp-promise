#pragma once

#include <iostream>
#include <functional>
#include <string>

#include "../event/EventLoop.hpp"
#include "IPromise.hpp"

template<typename Res, typename Rej>
class Promise: IPromise<Res, Rej> {
	public:
		Promise(std::function<void(IPromise<Res, Rej>&)> f) {
			this->function = f;
			EventLoop& eventLoop = EventLoop::getInstance();
			eventLoop.addPromise(*this);
		}

		void run() {
 			this->function(*this);
		}

		void then(std::function<void(Res)> fRes) {
			this->resolved = fRes;
		}

		void then(std::function<void(Res)> fRes, std::function<void(Rej)> fRej) {
			this->resolved = fRes;
			this->rejected = fRej;
		}

		void resolve(Res value) {
			this->resolved(value);
		};

		void reject(Rej error) {
			this->rejected(error);
		};

	private:
		std::function<void(IPromise<Res, Rej>&)> function;
		std::function<void(Res)> resolved = [](Res a) {};
		std::function<void(Rej)> rejected = [](Rej b) {};
};