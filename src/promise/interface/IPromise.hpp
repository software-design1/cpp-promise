#pragma once

template<typename Res = std::string, typename Rej = std::string>
class IPromise {
	public:
		virtual void resolve(Res) = 0;
		virtual void reject(Rej) = 0;
};