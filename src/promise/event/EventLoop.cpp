#include "EventLoop.hpp"
#include "../interface/Promise.hpp"

EventLoop::EventLoop()
{
	this->eventLoopThread = std::jthread(&EventLoop::run, this);
}

void EventLoop::addPromise(Promise<>& p)
{
	this->eventQueue.push(p);
}

void EventLoop::run()
{
	while (this->runner)
	{
		while(this->eventQueue.size() > 0)
		{
			auto promise = this->eventQueue.front();
			promise.run();
			this->eventQueue.pop();
		}
	}
}