#pragma once

#include <thread>
#include <iostream>
#include <queue>
#include <mutex>
#include <memory>

template<typename Res = std::string, typename Rej = std::string>
class Promise;

class EventLoop
{
public:
	static EventLoop& getInstance()
	{
		static auto instance = EventLoop();
		return instance;
	}

	void addPromise(Promise<>&);

private:
	bool runner = true;
	std::queue<Promise<>> eventQueue;
	std::jthread eventLoopThread;

	void run();

	EventLoop();
};